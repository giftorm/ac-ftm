import React from 'react'; 
 
import classes from './StoreItem.css';
import sampleImage from '../../../assets/images/grave_t.jpg';
import Item from '../Item/Item';

const storeItem = (props) => {
    console.log(props);
    return (
        <div onClick={props.clicked} className={ classes.gridItem }>
            <img className={classes.gridLogo} src={sampleImage} alt="cover" />
            <div className={ classes.Label }>
                <div className={ classes.labelData }>
                    <div className={ classes.labelHeader }>{props.itemName}</div>
                    <div className={ classes.labelDescription }>{props.description}</div>
                </div>
                <div className={ classes.labelPrice }>
                    <div className={ classes.labelPriceItem }>{props.price + " $"}</div>
                </div>
            </div>
            <Item title={props.itemName} id={props.id} description={props.description} />
        </div>
    );
}

export default storeItem;