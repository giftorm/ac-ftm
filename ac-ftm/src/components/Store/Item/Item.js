import React from 'react';

import classes from './Item.css';

const item = (props) => {
console.log(props);
console.log("HELLO WORLD");
    return (
        <div className={classes.Item}>
            <div className={classes.itemTitle}>
                <p>{props.title}</p>
            </div>
            <div className={classes.itemDescription}>
                <p>{props.description}</p>
            </div>
            <button>Add to cart</button>
        </div>
    );
};

export default item;