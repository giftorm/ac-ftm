import React, { Component } from 'react';

import classes from './Store.css';
import StoreItem from './StoreItem/StoreItem';
import Aux from '../../hoc/Ax/Ax';
import Item from './Item/Item';

class Store extends Component {
    state = {
        items: [{
            id: 5,
            cathegory: "cd",
            // picture: "sample",
            name: "monophobia",
            quantity: 8,
            description: "a heavy metal that is new an cool to the metal scene",
            price: 7.95
        },{
            id: 4,
            cathegory: "cd",
            // picture: "sample",
            name: "monophobia",
            quantity: 8,
            description: "a heavy metal that is new an cool to the metal scene",
            price: 7.95
        },{
            id: 6,
            cathegory: "cd",
            // picture: "sample",
            name: "monophobia",
            quantity: 8,
            description: "a heavy metal that is new an cool to the metal scene",
            price: 7.95
        },{
            id: 7,
            cathegory: "cd",
            // picture: "sample",
            name: "monophobia",
            quantity: 8,
            description: "a heavy metal that is new an cool to the metal scene",
            price: 7.95
        }],
        // block, item and list
        display: "shop"
    }

    displayHandler = (shopItems) => {
        console.log(this.state.display);
        let display = {};
        if (this.state.display === 'item') {
            display = <Item />
        } else {
            display = shopItems
        }
        return display;
    }

    toggleDisplayHandler = () => {
        if (this.state.display === 'shop') {
            this.setState({display: 'item'})
        } else {
            this.setState({display: 'shop'})
        }
    }

    render () {

        let shopItems = null
        if (this.state.items) {
            shopItems = (
                <Aux>
                    <StoreItem
                        id = {this.state.items[0].id}
                        itemName={this.state.items[0].name}
                        // picture={this.state.items[0].picture}
                        quantity={this.state.items[0].quantity}
                        price={this.state.items[0].price}
                        description={this.state.items[0].description}
                        clicked={this.toggleDisplayHandler} />
                </Aux>
            );
        }

        return (
            <div className={classes.Store}>
                {this.displayHandler(shopItems)}
            </div>
        );
    }
}

export default Store;