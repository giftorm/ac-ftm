import React from 'react';

import classes from './SocialMediaItem.css'
import Aux from '../../../hoc/Ax/Ax';

const socialMediaItem = (props) => (
    <Aux>
        <a
            className={[classes.SocialMediaItem, classes[props.btnType]].join(' ')} 
            href={props.hyperLink}>
            <i className={props.styleClass} alt={props.children} />
        </a>
    </Aux>
);

export default socialMediaItem