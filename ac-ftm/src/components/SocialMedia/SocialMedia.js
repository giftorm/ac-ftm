import React from 'react';

import classes from './SocialMedia.css'
import SocialMediaItem from './SocialMediaItem/SocialMediaItem';

const socialMedia = (props) => (
    <div className={classes.SocialMedia}>
        <SocialMediaItem 
            styleClass="fab fa-instagram fa-2x"
            hyperLink='https://www.instagram.com/forgettingthememories/'>Instagram</SocialMediaItem>       
        <SocialMediaItem 
            styleClass="fab fa-facebook-square fa-2x"
            hyperLink='https://www.facebook.com/Forgettingthememories/'>Facebook</SocialMediaItem>       
    </div>    
);

export default socialMedia;