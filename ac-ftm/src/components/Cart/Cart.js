import React from 'react';

import classes from './Cart.css';
import shoppingCart from '../../assets/images/shoppingCart.svg';

const cart = (props) => (
    <div className={classes.Cart}>
        <a href="/"><img src={shoppingCart} alt="Cart"/></a>
    </div>
);

export default cart;