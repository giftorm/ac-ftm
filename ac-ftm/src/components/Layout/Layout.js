import React from 'react';

import Aux from '../../hoc/Ax/Ax';
import Header from '../Header/Header';
import Store from '../Store/Store';
import Item from '../Store/Item/Item';

import { Route, Switch } from 'react-router-dom';

const Layout = (props) => (
    <Aux>
        <Header />
        <Switch>
            <Route path='/' exact />
            <Route path='/shop' component={Store} />
            <Route path='/item/:id' component={Item} />
        </Switch>
    </Aux>    
);

export default Layout;