import React from 'react';

import classes from './Header.css';
import SocialMedia from '../SocialMedia/SocialMedia';
import Toolbar from '../Navigation/Toolbar/Toolbar';

const header = (props) => (
    <div className={classes.HeaderArea}>
        <div className={classes.HeaderBackground}></div>
        <SocialMedia />
        <Toolbar />
    </div>
);

export default header;