import React from 'react';

import classes from './NavigationItems.css';
import NLink from '../../../UI/NLink/NLink';

const NLinks = () => (
    <div className={classes.NavigationItems}>
        <ul>
            {/* Because active is a boolean this way will suffice to make it true */}
            <NLink link="/" exact>Home</NLink>
            <NLink link="/shop/shirt" >T-Shirts</NLink>
            <NLink link="/shop/cd">CD's</NLink>
            <NLink link="/shop/hoodies">Hoodies</NLink>
            <NLink link="/shop/bundles">Bundles</NLink>
        </ul>
    </div>
);

export default NLinks;