import React from 'react';

import NavigationItems from '../NavigationItems/NavigationItems';
import classes from './Toolbar.css';

const toolbar = (props) => (
    <div className={classes.Toolbar}>
        <header>
            <nav>
                <NavigationItems />
            </nav>
        </header>    
    </div>
);

export default toolbar;