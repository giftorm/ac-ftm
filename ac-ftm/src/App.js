import React, { Component } from 'react';

import Aux from './hoc/Ax/Ax';
import ForgettingTheMemories from './containers/ForgettingTheMemories';

class App extends Component {
  render() {
    return (
      <Aux>
        <ForgettingTheMemories />
      </Aux>
    );
  }
}

export default App;
