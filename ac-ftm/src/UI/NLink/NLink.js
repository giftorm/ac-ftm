import React from 'react';

import classes from './NLink.css';
import { NavLink } from 'react-router-dom';

// Navigation link item for navbar
const nLink = (props) => (
    <li className={classes.NLink}>
        <NavLink
            to={props.link}
            exact={props.exact}
            activeClassName={classes.active}>{props.children}
        </NavLink>
    </li>
);

export default nLink;